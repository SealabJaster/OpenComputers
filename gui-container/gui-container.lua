local component = require("component")
local gpu = component.gpu
local event = require("event")

local api = {}

api.containerAPI = {}
api.containerAPI.__index = api.containerAPI

api.textAPI = {}
api.textAPI.__index = api.textAPI

api.buttonAPI = {}
api.buttonAPI.__index = api.buttonAPI

-- api funcs
function api.newContainer(x, y, width, height)
  checkArg(1, x, "number")
  checkArg(2, y, "number")
  checkArg(3, width, "number")
  checkArg(4, height, "number")

  local container = setmetatable({}, api.containerAPI)
  container.x = x
  container.y = y
  container.width = width
  container.height = height
  container.gpu = gpu
  container.children = {}
  container.children.all = {}
  container.children.clickable = {}

  return container
end

-- Container API
function api.containerAPI:addChild(child)
  checkArg(1, child, "table")

  local canAdd = true
  for i, v in ipairs(self.children.all) do
    if v == child then canAdd = false end
  end

  if canAdd then
    child.container = self
    table.insert(self.children.all, child)
  end
end

function api.containerAPI:addClickable(child)
  self:addChild(child)
  table.insert(self.children.clickable, child)
end

-- x and y are 1-based
function api.containerAPI:constrainSize(x, width, y, height)
  x = x or 1
  width = width or 1
  y = y or 1
  height = height or 1

  if width > self.width then
    width = self.width - (x - 1)
  end

  if height > self.height then
    height = self.height - (y - 1)
  end

  return width, height
end

function api.containerAPI:drawText(x, y, text, foreground, background)
  checkArg(1, x, "number")
  checkArg(2, y, "number")
  checkArg(3, text, "string")

  background = background or 0x000000
  foreground = foreground or 0xFFFFFF

  local trueX = self.x + (x - 1)
  local trueY = self.y + (y - 1)
  
  -- Keep the text in the bounds of the container
  local textWidth = (x - 1) + text:len() -- grr, 1-based index
  textWidth = self:constrainSize(x, textWidth)

  local gpu = self.gpu
  gpu.setBackground(background)
  gpu.setForeground(foreground)
  gpu.set(trueX, trueY, text:sub(1, textWidth + 1))
end

function api.containerAPI:pollEvents()
  while true do
    local _, _, x, y, button, player = event.pull(0, "touch")
    if _ == nil then break end

    for i, v in ipairs(self.children.clickable) do
      local trueX = self.x + v.x - 1
      local trueY = self.y + v.y - 1
      local wasClicked = (x >= trueX and x <= (trueX + v.width - 1) and y >= trueY and y <= (trueY + v.height - 1))

      if wasClicked and v.onClick ~= nil then
        v.onClick(x, y, button, player)
      end
    end
  end
end

-- Text API
function api.containerAPI:newLabel(x, y, text, foreground, background)
  local label = setmetatable({}, api.textAPI)
  label.x = x or 1
  label.y = y or 1
  label.text = text or ""
  label.foreground = foreground or 0xFFFFFF
  label.background = background or 0x000000
  
  self:addChild(label)
  return label
end

function api.textAPI:draw()
  self.container:drawText(self.x, self.y, self.text, self.foreground, self.background)
end

-- Button API
function api.containerAPI:newButton(x, y, width, height, text, onClick, foreground, background)
  local button = setmetatable({}, api.buttonAPI)
  button.x = x or 1
  button.y = y or 1
  button.width = width or 6
  button.height = height or 3
  button.onClick = onClick
  button.foreground = foreground or 0xFFFFFF
  button.background = background or 0x00000
  button.text = text or ""

  self:addClickable(button)
  return button
end

function api.buttonAPI:draw()
  local trueX = self.container.x + (self.x - 1)
  local trueY = self.container.y + (self.y - 1)
  local width, height = self.container:constrainSize(self.x, self.width, self.y, self.height)

  local gpu = self.container.gpu
  gpu.setBackground(self.background)
  gpu.setForeground(self.foreground)
  gpu.fill(trueX, trueY, width, height, " ")

  local textX = trueX + math.floor(width / 2) - math.floor(self.text:len() / 2)
  local textY = trueY + math.floor(height / 2)
  gpu.set(textX, textY, self.text) -- TODO bother to cut off the text if it's bigger than the button
end

return api